import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],   
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  const obj = {
    winner: squares[0],
    a: -1,
    b: -1,
    c: -1   
  };
  for(let i=0; i<lines.length; i++) {
    const [a, b, c] = lines[i];
    if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      obj.winner = squares[a];
      obj.a = a;
      obj.b = b;
      obj.c = c;
      return obj;
    }
  }
  return null;
}

function Square(props) {
  const color1 = '#00b33c';
  const color2 = '#000000';
  let mystyle;
  if(props.a === props.i|| props.b === props.i || props.c === props.i)
    mystyle = {color: color1}
  else 
    mystyle = {color: color2}

  return (
    <button className="square" onClick={props.onClick} style={mystyle}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {

  renderSquare(i) {
    return (
      <Square 
        value={this.props.squares[i]} 
        a={this.props.a}
        b={this.props.b}
        c={this.props.c}
        i={i}
        onClick={() => this.props.onClick(i) } 
      />
    );
  }

  render() {

    var rows = [];
    for(var i=0; i < 3; i++) {
      rows.push(
        <div key={i} className="board-row">
          {this.renderSquare(i*3 + 0)}
          {this.renderSquare(i*3 + 1)}
          {this.renderSquare(i*3 + 2)}
        </div>);
    }
    return <div>{rows}</div>;
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        x: 0,
        y: 0
      }],
      xIsNext: true,
      stepNumber: 0,
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[this.state.stepNumber];
    const squares = current.squares.slice();
    if(squares[i] || calculateWinner(squares)) 
      return;
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState(
      {
        history: history.concat([{
          squares: squares,
          x: Math.floor(i%3) + 1,
          y: Math.floor(i/3) + 1
        }]),
        xIsNext: !this.state.xIsNext,
        stepNumber: history.length,
      }
    );   
  }

  jumpTo(step) {
    this.setState ( {
      stepNumber: step,
      xIsNext: (step%2) === 0
    });
  }

  render() {
    const history = this.state.history;
    const current = history[history.length - 1];
    const winner = calculateWinner(current.squares);
    const moves = history.map((step, move) => {
      const desc = move ? 'Go to move #' + move + ' (' + step.x + ', ' + step.y + ')' : 'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)} > {desc} </button>
        </li>
      );
    });

    let status, a, b, c;
    if (winner && winner.winner) {
      status = 'Winner: ' + winner.winner;
      a = winner.a;
      b = winner.b;
      c = winner.c;
    }
    else if(this.state.stepNumber === 9)
      status = 'draw :(';
    else
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');

    return (
      <div className="game">
        <div className="game-board">
        <Board
            squares={current.squares}
            a={a}
            b={b}
            c={c}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
